<?php
/*
*admin.php 
*copyright 2010 Pickle Inc
*Coded By Pickle
*/
require "body.php";
?>
<script type="text/javascript">
var xmlhttp;

function showUser(str)
{
xmlhttp=GetXmlHttpObject();
if (xmlhttp==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var url="getuser.php?type=admin";
url=url+"&q="+str;
url=url+"&sid="+Math.random();
xmlhttp.onreadystatechange=stateChanged;
xmlhttp.open("GET",url,true);
xmlhttp.send(null);
}

function stateChanged()
{
if (xmlhttp.readyState==4)
{
document.getElementById("input").innerHTML=xmlhttp.responseText;
}
}

function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}
</script>
<?php
if (!isset($_SESSION['id'])) { print "Go <a href='login.php'>Login</a>"; }
else {
 $id=$_SESSION['id'];
$result = mysqli_query($c,"SELECT * FROM users WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array($result);
if ($row['level'] != 2) { print "GET OUT"; }
else {
//display list
if (!isset($_GET['action'])) {
print "<table border='1' width='100'>
<tr><th>Admin panel</th></tr>
<tr><td><a href='admin.php?action=edituser'>Edit users</a></td></tr>
<tr><td><a href='admin.php?action=ban'>IP Ban</a></td></tr>
<tr><td><a href='admin.php?action=news'>News</a></td></tr>
</table>";
}
switch ($_GET['action']) {
//edit user
case edituser:
$result = mysqli_query($c,"SELECT * FROM users") or die(mysqli_error($c));
while ($row = mysqli_fetch_array($result)) {
if ($row['id'] == $_GET['id']) { 
$html=$html . "<option value='" . $row['user'] . "' selected='selected'>" . $row['user'] . "</option>"; 
print "<body onLoad='showUser(". $row['user'] .")'></body>";
}
else {
$html=$html . "<option value='" . $row['user'] . "'>" . $row['user'] . "</option>";
}
}
print "<form method='post' action='admin.php?action=edituser'>
User:<select name='user' onChange='showUser(this.value)'>
" . $html . "
</select>
<br />
<div id='input'></div>
</form>
</body>";

break;

//ban ip
case ban:

break;

//news
case news:
//news table
if (isset($_POST['news'])) { 
//update
if (isset($_POST['oldcode'])) {
$nid=htmlspecialchars($_POST['id']);
$msg=htmlspecialchars($_POST['oldcode']);
if (isset($msg)) {
mysqli_query($c,"UPDATE news SET msg='$msg' WHERE id='$nid' ") or die(mysqli_error($c));
print "Edited News";
}
}
//insert
elseif (isset($_POST['newcode'])) {
$msg=htmlspecialchars($_POST['newcode']);
mysqli_query($c,"INSERT INTO news (user_id, msg, time)
VALUES ('$id', '$msg', NOW())") or die(mysqli_error($c));
print "Added News";
}
}
//edit,delete, add news
print "<table border='1' width='150'>
<tr><th colspan='2'>News</th></tr>";
 $result = mysqli_query($c,"SELECT * FROM news ORDER BY time DESC") or die(mysqli_error($c));
while ($row = mysqli_fetch_array($result)) {
if ($fancy == 1) { $class="class='alt'"; $fancy=0; }
else { $fancy=1; }
print "<form method='POST' action='admin.php?action=news'>
<tr><td ". $class ."><a href='admin.php?action=delnews&id=". $row['id'] ."'>Delete news</a>
<hr/><input type='submit' name='news' value='Edit' /></td>
<input type='hidden' name='id' value='". $row['id'] ."' />
<td ". $class ."><textarea name='oldcode' rows='10' cols='50'>". $row['msg'] ."</textarea></td></tr>
</form>
";
}
if ($fancy == 1) { $class="class='alt'"; }
print "<tr><th colspan='2'>Add News</th></tr>
<form method='POST' action='admin.php?action=news'>
<tr><td ". $class ."><input type='submit' name='news' value='Add' /></td>
<td ". $class ."><textarea name='newcode' rows='10' cols='50'></textarea></td></tr>
</form>
</table>";

break;

//delete news
case delnews:
$nid=htmlspecialchars($_GET['id']);
mysqli_query($c,"DELETE FROM news WHERE id='$nid' ") or die(mysqli_error($c));
print "Deleted News<META http-equiv='refresh' content='1;URL=admin.php?action=news'>";
break;

}

print "<a href='admin.php'>home</a>";
}
}
require "footer.php";
?>
