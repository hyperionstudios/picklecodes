<?php
/*
*body.php 
*copyright 2010 Pickle Inc
*Coded By Pickle
*/
require "mysql.php";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>PicklesCodes</title>
<meta content="A scripting Website" name="description" />
<meta content="Programming,scripting,coding" name="keywords" />
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" />
<meta content="en-us" http-equiv="Content-Language" />
<meta content="General" name="rating" />
<meta content="no" http-equiv="imagetoolbar" />
<meta content="Copyright � 2010,  Pickle All Rights Reserved" name="copyright" />
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="container">
	<div id="banner">
	<div align='center'><img src='images/logo.png' alt="Pickle's Scripting Site"></div>
	<?php
  $result = mysqli_query($c, "SELECT NOW()") or die(mysqli_error($c));
$row= mysqli_fetch_array( $result);
?>
<h5 align='right'><?php print $row['NOW()']; ?><br /></a></h5>
</div>
<div id="topmenu">
<ul>
<?php
 if (isset($_SESSION['id'])) {
print "<li><a href='index.php'>Home</a></li>
<li><a href='profile.php?id=". $_SESSION['id'] ." '>My Profile</a></li>";
  $result = mysqli_query($c, "SELECT * FROM mail WHERE user_to='". $_SESSION['id'] ." ' ") or die(mysqli_error($c));
while ($row= mysqli_fetch_array( $result)) {
$m++;
}
if ($m >= 0) { $m=0; }
if ($row['state'] == 1) { $mail="<font color='red'>". $m ."</font>"; }
else { $mail=$m; }
print "<li><a href='inbox.php?action=inbox'>My Inbox [". $mail ."]</a></li>"; 
  $result = mysqli_query($c, "SELECT * FROM users WHERE id='". $_SESSION['id'] ."' ") or die(mysqli_error($c));
$row= mysqli_fetch_array( $result);
if ($row['level'] == 2) {  print "<li><a href='admin.php'>Admin Panel</a></li>"; }
print "<li><a href='snippet.php?action=post'>Post Snippet</a></li>
<li><a href='logout.php'>Logout</a></li>
";
}
else {
print "<li><a href='index.php'>Home</a></li>
<li><a href='login.php'>Login</a></li>
<li><a href='register.php'>Register</a></li>"; 
}
?>
<li><a href='forum/index.php'>Forum</a></li> 
</ul>
</div>
<div id="leftnav">
  <ul>
	 <li><strong>Main Menu</strong></li>
<?php 
if (!isset($_SESSION['id'])) {
print "<li><strong>Login</strong></li>
<form method='POST' action='check_login.php'>
<li>User: <input type='text' name='user'></li>
<li>Pass: <input type='password' name='pass'></li>
<li><input type='submit' name='login' value='Login' /></li>
</form>";
}
else {
print "<li><strong>Welcome ". $_SESSION['user'] ."</strong></li>";
}
?>
<li><strong>Scripting resources</strong></li>
<li><a href="pastebin.php">Pastebin</a></li>
<li><a href="snippet.php?action=post">Post a Snippet</a></li>
<li><strong>About P.S.S.</strong></li>
<li><a href="about.php">About</a></li>
<li><a href="ToS.php">Terms and Services</a></li>
<li><strong>Latest Pastes</strong></li>
<?php
//latest pastes
 $result = mysqli_query($c, "SELECT * FROM paste ORDER BY time DESC LIMIT 0, 10") or die(mysqli_error($c));
while ($row = mysqli_fetch_array( $result)) {
if ($row['user_id'] == 0) { $user="Guest"; }
else {
 $result2 = mysqli_query($c, "SELECT * FROM users WHERE id='". $row['user_id'] ."' ") or die(mysqli_error($c));
$row2 = mysqli_fetch_array( $result2);
$user=$row2['user'];
if (!isset($user)) {  $user="Guest"; }
}
print "<li><sup>". $row['time'] ."</sup>
<br/><b><a href='pastebin.php?id=". $row['id'] ."'>". $user ."</a></b></li>";
}
?>
    </ul>
	</div>
	<div id="rightnav">
	<ul>
<!--- Search --->
<li><strong>Search:<input type='text' name='item'></strong></li>
<form method='GET' action='search.php'>
<li><input type='submit' name='search' value='Search'></li>
</form>
<li><strong>Headlines</strong></li>
<?php
//Headlines
 $result = mysqli_query($c, "SELECT * FROM news ORDER BY time DESC LIMIT 0, 10") or die(mysqli_error($c));
while ($row = mysqli_fetch_array( $result)) {
$str=$row['msg'];
$news=str_word_count($str, 1);
$m=0;
$msg="";
while ($m  != 5) {
$msg=$msg ." ". $news[$m];
$m++;
}
print "<li><sup>". $row['time'] ."</sup>
<br/><b><a href='news.php#". $row['id'] ."'>". $msg."</a></b></li>";
}
?>
<li><strong>Latest Snippets</strong></li>
<?php
//latest snippets
 $result = mysqli_query($c, "SELECT * FROM snippet ORDER BY time DESC LIMIT 0, 15") or die(mysqli_error($c));
while ($row = mysqli_fetch_array( $result)) {
print "<li><sup>". $row['time'] ."</sup>
<br><sup>". $row['snippet_type'] ." snippet</sup>
<br><b><a href='snippet.php?id=". $row['id'] ."'>". $row['snippet_name'] ."</a></b></li>";
}
?>
</ul>
	</div>
	<div id="content">
	
