<?php
/*
*check_login.php 
*copyright 2010 Pickle Inc
*Coded By Pickle
*/
require "body.php";

if (isset($_POST['login'])) {
$user=mysqli_real_escape_string($c,$_POST['user']);
$pass=mysqli_real_escape_string($c,Encrypt($_POST['pass']));
if (!$user) { print "<h3 align='center'>You did not enter a username. <br><a href='login.php'>Go Back</a></h3>"; }
else if (!$pass) { print "<h3 align='center'>No password was entered. <br><a href='login.php'>Go Back</a></h3>"; }
else {
  $result = mysqli_query($c,"SELECT * FROM users WHERE user='$user'") or die(mysqli_error($c));
$row = mysqli_fetch_array($result);
if (isset($row['id'])) {
if ($pass != $row['pass']) { print"<h3 align='center'>Password incorrect. <br><a href='login.php'>Go Back</a></h3>"; }
 else {
 //Check to see if were are banned!
  $result2 = mysqli_query($c,"SELECT * FROM ban WHERE ip='". $_SERVER['REMOTE_ADDR'] ."'") or die(mysqli_error($c));
$row2 = mysqli_fetch_array( $result2);
$id=$row2['id'];
if (isset($id)) {
print "<p><h3 align='center'><font color='red'>Looks like you have been IP banned From the Website! Reason: ". $row2['reason'] ."</font></h3></p>
<p>If you have any problems Please go to the <a href='http://forum.php' target='_BLANK'>Forums</a> or complain to the admins at
<a href='mailto:Admin@PicklesCode.co.cc'>Admin@PicklesCode.co.cc</a></p>"; 
}
else if (!isset($id)) {
$_SESSION['id']=$row['id'];
mysqli_query($c,"UPDATE users SET ip='". $_SERVER['REMOTE_ADDR'] ."' WHERE user='$user'") or die(mysqli_error($c));
print "<h3 align='center'>Logged in</h3>
<META http-equiv='refresh' content='1;URL=index.php'>";
}
}
}
else {
print "<h3 align='center'>Username is not registered</h3>
<META http-equiv='refresh' content='1;URL=login.php'>";
}
}
}
require "footer.php";
?>
