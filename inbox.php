<?php
/*
*inbox.php 
*copyright 2010 Pickle Inc
*Coded By Pickle
*/
require "body.php";

if (!$_SESSION['id']) { die("<META http-equiv='refresh' content='1;URL=login.php'>"); }

switch ($_GET['action']) {
//Inbox
case inbox:
//restrict number of mail on page
$page=mysqli_real_escape_string($c,($_GET['page']);
$per_page = 25;

if (!$page) {
$page = 1;
}
$prev_page = $page - 1;
$next_page = $page + 1;
$query = mysqli_query("SELECT * FROM mail WHERE user_to='". $_SESSION['id'] ."' ");
$page_start = ($per_page * $page) - $per_page;
$num_rows = mysql_num_rows($query);
if ($num_rows <= $per_page) {
$num_pages = 1;
} 
else if (($num_rows % $per_page) == 0) {
$num_pages = (int)($num_rows / $per_page);
} 
else {
$num_pages = (int)($num_rows / $per_page) + 1;
}
print "<p align='right'>";
if ($num_pages > 1) {
if ($page > 1) {
print "<a href='inbox.php?action=inbox&page=". ($page - 1) ."'>&larr;</a> ";
}
print "<b>".$page."</b>"; 
if ($page < $num_pages) {
print "<a href='inbox.php?action=inbox&page=". ($page + 1) ."'>&rarr;</a>";
}
print "<form method='GET' action='inbox.php?action=inbox'>
<input type='text' name='page' size='2'/> <input type='submit' value='Go To'/>
</form>";
 }
print "Pages: ".$num_pages."</p>";
//begin table
print "<p>Red Bold Subjects means that they are unread</p>
<table border='1' align='center'>
<tr>
<th colspan='2' class='fancy'><a href='inbox.php?action=inbox'>Inbox</a></th>
<th colspan='2' class='fancy'><a href='inbox.php?action=outbox'>outbox</a></th>
</tr>
<form method='post' action='inbox.php?action=delete&form=1'>
<tr>
<td class='fancy'><input type='checkbox' name='check' value='all' title='Delete All'/></td>
<td class='fancy'><b>From:</b></td>
<td class='fancy'><b>Subject:</b></td>
<td class='fancy'><b>Time:</b></td>
</tr>";
$result = mysqli_query("SELECT * FROM mail WHERE user_to='". $_SESSION['id'] ."'  ORDER BY time desc ") or die(mysqli_error());
while($row = mysqli_fetch_array( $result)) {
$id=$_SESSION['id'];
$from=$row['user_from'];
$subject=$row['subject'];
$msg=$row['msg'];
 $state=$row['state'];
if ($state == 1) { $subject="<font color='red'><b>". $subject ."</b></font>"; }
$time=$row['time'];
 $mailid=$row['id'];
 
 $result2 = mysqli_query("SELECT * FROM users WHERE ID = '$from' ") or die(mysqli_error());
$row = mysqli_fetch_array( $result2);
$fromu=$row['user'];
$count++;
if ($fancy == 1) {
print "<tr>
<td class='fancy'><input type='checkbox' name='check". $count ."' value='". $mailid ."' title='Delete ". $mailid ."'/></td>
<td class='fancy'><a href='profile.php?id=" . $from . "'>" . $fromu . "</a></td>
<td class='fancy'><a href='inbox.php?action=read&id=". $mailid ."'>". $subject ."</td>
<td class='fancy'>" . $time . "</td>
</tr>";
$fancy=0;
}
else {
print "<tr>
<td class='alt'><input type='checkbox' name='check". $count ."' value='". $mailid ."' title='Delete ". $mailid ."'/></td>
<td class='alt'><a href='profile.php?id=" . $from . "'>" . $fromu . "</a></td>
<td class='alt'><a href='inbox.php?action=read&id=". $mailid ."'>". $subject ."</td>
<td class='alt'>" . $time . "</td>
</tr>";
$fancy=1;
}
}
if (!$from) {
print "<tr><th class='fancy' colspan='4'>You has no messages.</th></tr>";
}
else {
print "<input type='hidden' name='checkboxs' value='". $count ."'/>
<tr>
<th colspan='4' class='fancy'><div align='left'><input type='submit' name='Submit' value='Delete'/></div></th>
</tr>";
}
print"</form>
</table>";
break;


//Sent Messages
case outbox:
//restrict number of mail on page
$page=mysqli_real_escape_string($c,($_GET['page']);
$per_page = 25;

if (!$page) {
$page = 1;
}
$prev_page = $page - 1;
$next_page = $page + 1;
$query = mysqli_query("SELECT * FROM mail WHERE user_from='". $_SESSION['id'] ."' ");
$page_start = ($per_page * $page) - $per_page;
$num_rows = mysqli_num_rows($query);
if ($num_rows <= $per_page) {
$num_pages = 1;
} 
else if (($num_rows % $per_page) == 0) {
$num_pages = (int)($num_rows / $per_page);
} 
else {
$num_pages = (int)($num_rows / $per_page) + 1;
}
print "<p align='right'>";
if ($num_pages > 1) {
if ($page > 1) {
print "<a href='inbox.php?action=outbox&page=". ($page - 1) ."'>&larr;</a> ";
}
print "<b>".$page."</b>"; 
if ($page < $num_pages) {
print "<a href='inbox.php?action=outbox&page=". ($page + 1) ."'>&rarr;</a>";
}
print "<form method='GET' action='inbox.php?action=outbox'>
<input type='text' name='page' size='2'/> <input type='submit' value='Go To'/>
</form>";
 }
print "Pages: ".$num_pages."</p>";
//begin table
print "<p>Red Bold Subjects means that they are unread</p>
<table border='1' align='center'>
<tr>
<th colspan='2' class='fancy'><a href='inbox.php?action=inbox'>inbox</a></th>
<th colspan='2' class='fancy'><a href='inbox.php?action=outbox'>Outbox</a></th>
</tr>
<tr>
<td class='fancy'><b>To:</b></td>
<td class='fancy'><b>Subject:</b></td>
<td class='fancy'><b>Time:</b></td>
</tr>";
 $result = mysqli_query("SELECT * FROM mail WHERE user_from='". $_SESSION['id'] ."'  ORDER BY time desc ") or die(mysqli_error());
while($row = mysqli_fetch_array( $result)) {
$id=$row['user_from'];
$to=$row['use_to'];
$subject=$row['subject'];
$msg=$row['msg'];
$state=$row['state'];
if ($state == 1) { $subject="<font color='red'><b>". $subject ."</b></font>"; }
	$time=$row['time'];
 $mailid=$row['id'];
 $result2 = mysqli_query("SELECT * FROM users WHERE ID='$to' ") or die(mysqli_error());
$row = mysqli_fetch_array( $result2);
$tou=$row['user'];
if ($state == 1) { print "<font color='red'>"; }
if ($fancy == 1) {
print "<tr>
<td class='fancy'><a href='profile.php?id=" . $to . "'>" . $tou . "</a></td>
<td class='fancy'><a href='inbox.php?action=read&id=". $mailid ."'>" . $subject . "</a></td>
<td class='fancy'>" . $time . "</td>
</tr>"; 
$fancy=0;
}
else {
print "<tr>
<td class='alt'><a href='profile.php?id=" . $to . "'>" . $tou . "</a></td>
<td class='alt'><a href='inbox.php?action=read&id=". $mailid ."'>" . $subject . "</a></td>
<td class='alt'>" . $time  . "</td>
</tr>"; 
$fancy=1;
}
if ($state == 1) { print "</font>"; }
}
if (!$to) {
print "<tr><th class='fancy' colspan='3'>You had sent no messages.</th></tr>";
}
print "</table>";
break;

//Read Message
case read:
$mailid=mysqli_real_escape_string($c,($_GET['id']);
$result = mysqli_query("SELECT * FROM mail WHERE id='$mailid'") or die(mysqli_error());
$row = mysqli_fetch_array( $result);
 if ($row['user_to'] == $_SESSION['id']) { mysqli_query("UPDATE mail SET state='0' WHERE id='$mailid'") or die(mysqli_error()); }
$from=$row['user_from'];
$to=$row['user_to'];
if ($to == $_SESSION['id'] || $from == $_SESSION['id']) {
$subject=$row['subject'];
$msg=$row['msg'];
$time=$row['time'];

 $result = mysqli_query("SELECT * FROM users WHERE id='$from' ") or die(mysqli_error());
$row = mysqli_fetch_array( $result);
$fromu=$row['user'];
 $result = mysqli_query("SELECT * FROM users WHERE id='$to' ") or die(mysqli_error());
$row = mysqli_fetch_array( $result);
$tou=$row['user'];

print "<table border='1' align='center'>
<tr>
<th class='fancy'><a href='inbox.php?action=inbox'>inbox</a></th>
<th class='fancy'><a href='inbox.php?action=outbox'>outbox</a></th>
</tr>
<tr>
<td colspan='2' class='alt'>
<b>To:</b> <a href='nation.php?id=" . $to . "'>" . $tou . "</a>
&ensp; &ensp; &ensp;<b>From:</b> <a href='profile.php?id=" . $from . "'>" . $fromu . "</a>
&ensp; &ensp; &ensp;<i>" . $time . "</i><br/>
<b>Subject:</b>&ensp; ". $subject ."<hr/>
<div align='center'>". $msg ."</div>
</tr>
<tr>
<td class='fancy'><div align='center'><a href='inbox.php?action=delete&id=". $mailid ."'>Delete</a></div></td>
<td class='fancy'><div align='center'><a href='inbox.php?action=reply&id=". $mailid ."'>Reply</a></div></td>
</tr>";
}
print "</table>";
break;

//Send Messages
case send:
if (isset($_POST['msg'])) {
$to=mysqli_real_escape_string($c,($_GET['id']);
$from=$_SESSION['id'];
$msg=mysqli_real_escape_string($c,($_POST['msg']);
$subject=mysqli_real_escape_string($c,($_POST['subject']);
if (!$msg) { die("<h3 align='center'>you need to enter a message..  <a href='inbox.php?action=send&id=" . $to . "'>Go Back</a></h3>"); }
if (!$subject) { die("<h3 align='center'>you need to enter a subject..  <a href='inbox.php?action=send&id=" . $to . "'>Go Back</a></h3>"); }

 mysqli_query("INSERT INTO mail (user_from, user_to, msg, subject, time, state)
VALUES ('$from', '$to', '$msg', '$subject', NOW(), '1')");

print "<p align='center'>Message Sent.</p>
<META http-equiv='refresh' content='1;URL=inbox.php?action=inbox'>";
}

else {
$result = mysqli_query("SELECT * FROM users WHERE id='". mysqli_real_escape_string($c,($_GET['id']) ."'") or die(mysqli_error());
$row2 = mysqli_fetch_array( $result);
if (!isset($row2['id'])) {
print "<p align='center'>User dosen't exist</p><META http-equiv='refresh' content='1;URL=inbox.php?action=inbox'>";
}
else {
print "<table border='1' align='center'>
<form action='inbox.php?action=send&id=" . $_GET['id'] . "' method='post'>
<tr>
<th colspan='2' class='fancy'>Send ". $row['user'] ." a Message!</th>
</tr>
<tr>
<td class='alt'>Subject:</td>
<td class='alt'><input type='text' name='subject' width='25'><td>
</tr>
<tr>
<td class='fancy'>Message</td>
<td class='fancy'><textarea rows='5' cols='20' name='msg'></textarea></td>
</tr>
<tr>
<td class='alt'><input type='reset' value='Reset'></td>
<td class='alt'><div align='right'><input type='submit' value='Mail!'></div></td>
</tr>
</form>
</table>";
}
}
break;
//Reply Messages
case reply:
if (isset($_POST['msg'])) {
$to=mysqli_real_escape_string($c,($_GET['id']);
$from=$_SESSION['id'];
$msg=mysqli_real_escape_string($c,($_POST['msg']);
$subject=mysqli_real_escape_string($c,($_POST['subject']);
if (!$msg) { die("<h3 align='center'>you need to enter a message..  <a href='inbox.php?action=reply&id=" . $to . "'>Go Back</a></h3>"); }
if (!$subject) { die("<h3 align='center'>you need to enter a subject..  <a href='inbox.php?action=reply&id=" . $to . "'>Go Back</a></h3>"); }

 mysqli_query("INSERT INTO mail (userfrom, userto, msg, subject, time, state)
VALUES ('$from', '$to', '$msg', '$subject', NOW(), '1')");

print "<p align='center'>Message Replied.</p>
<META http-equiv='refresh' content='1;URL=inbox.php?action=inbox'>";
}

else {
$result = mysqli_query("SELECT * FROM mail WHERE id='". mysqli_real_escape_string($c,($_GET['id']) ."'") or die(mysqli_error());
$row = mysqli_fetch_array( $result);
if (!isset($row['id'])) {
print "<p align='center'>Message dosen't exist</p><META http-equiv='refresh' content='1;URL=inbox.php?action=inbox'>";
}
else {
$result = mysqli_query("SELECT * FROM users WHERE id='". mysqli_real_escape_string($c,($row['userfrom']) ."'") or die(mysqli_error());
$row2 = mysqli_fetch_array( $result);
if (!isset($row2['id'])) {
print "<p align='center'>User dosen't exist</p><META http-equiv='refresh' content='1;URL=inbox.php?action=inbox'>";
}
else {
print "<table border='1' align='center'>
<form action='inbox.php?action=reply&id=" . $_GET['id'] . "' method='post'>
<tr>
<th colspan='2' class='fancy'>Reply to ". $row2['user'] ."'s Message!</th>
</tr>
<tr>
<td class='alt'>Subject:</td>
<td class='alt'><input type='text' name='subject' value='Reply: ". $row['subject']."' width='25'><td>
</tr>
<tr>
<td class='fancy'>Message</td>
<td class='fancy'><textarea rows='5' cols='20' name='msg'>\"". $row['msg'] ."\"</textarea></td>
</tr>
<tr>
<td class='alt'><input type='reset' value='Reset'></td>
<td class='alt'><div align='right'><input type='submit' value='Mail!'></div></td>
</tr>
</form>
</table>";
}
}
break;
}
//Delete Messages
case delete:
if ($_GET['form'] == 1) {
if ($_POST['check'] == "all") {
  mysqli_query("DELETE FROM mail WHERE user_to='". $_SESSION['id'] ."'") or die(mysqli_error());
  print "All messages Deleted";
}
else {
while ($count <= $_POST['checkboxs']) {
$count++;
$check=$_POST['check'. $count];
if (isset($check)) {
mysqli_query("DELETE FROM mail WHERE id='$check'") or die(mysqli_error());
}
}
print"Deleted Message(s)";
}
}
else {
 if (isset($_GET['id'])) {
 $result = mysqli_query("SELECT * FROM mail WHERE id='". mysqli_real_escape_string($c,$_GET['id']) ."'") or die(mysqli_error());
$row = mysqli_fetch_array( $result);
$id=$row['user_to'];
if ($_SESSION['id'] == $id) {
mysqli_query("DELETE FROM mail WHERE id='". $_GET['id'] ."'") or die(mysqli_error());

print "<p align='center'>Message Deleted.</p>";
}
}
}

print "<META http-equiv='refresh' content='1;URL=inbox.php?action=inbox'>";
break;
}

require "footer.php";
?>
