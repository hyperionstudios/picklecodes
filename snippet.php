<?php 
/*
*snippet.php 
*copyright 2010 Pickle Inc
*Coded By Pickle
*/
require "body.php";
$time=date("U");
$uid=$_SESSION['id'];
?>
<script type="text/javascript">
function SelectAll(id)
{
    document.getElementById(id).focus();
    document.getElementById(id).select();
}
</script>

<?php
if (isset($_SESSION['id'])) { 
switch ($_GET['action']) {
//new snippet
case post:

if (isset($_POST['post'])) {
$subject=mysqli_real_escape_string($c,htmlspecialchars($_POST['subject']));
$type=mysqli_real_escape_string($c,htmlspecialchars($_POST['lang']));
$code=mysqli_real_escape_string($c,htmlspecialchars($_POST['snippet']));
if ($code == "") { print "You need to enter a code"; }
elseif ($subject == "") { print "You need to enter a Name"; }
else {
if ($type == "") { $type="mIRC"; }
if ($type != "mIRC" && $type != "PHP" && $type != "ASP" && $type != "Java" && $type !="C" && $type != "C++") {
print "Hacker!";
$type="mIRC";
}

mysqli_query($c,"INSERT INTO snippet (user_id, snippet_name, snippet_type, snippet_code, time) 
VALUES ('$uid', '$subject', '$type', '$code', NOW())") or die(mysqli_error($c));
  $result = mysqli_query($c,"SELECT * FROM snippet WHERE user_id='$uid' AND time=NOW() ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);

print "Posted Snippet. <META http-equiv='refresh' content='1;URL=snippet.php?id=". $row['id'] ."'>"; 
}
}
//display posting table
else {
print "
<form method='POST' action='snippet.php?action=post'>
<p><h2>Post a Snippet</h2>
Select Language:
<select name='lang'>
<option value='mIRC'>mIRC</option>
<option value='PHP'>PHP</option>
<option value='ASP'>ASP</option>
<option value='Java'>Java</option>
<option value='C'>C</option>
<option value='C++'>C++</option>
</select>
<br>
Snippet Name:<input type='text' name='subject'><br>
<textarea name='snippet' rows='30' cols='50'></textarea><br>
<input type='submit' name='post' value='Post'>
</p>
</form>";
}

break;

//comment snippet
case comment:

$id=$_GET['id'];
$comment=htmlspecialchars($_POST['comment']);
if ($comment == "") { print "You can't comment nothing!"; }
else {
  $result = mysqli_query($c,"SELECT * FROM snippet WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);
if (isset($row['id'])) {
mysqli_query($c,"INSERT INTO comments (user_id, snippet_id, msg, time) 
VALUES ('$uid', '$id', '$comment',  NOW())") or die(mysqli_error($c));
print "Commented"; 
}
}

print "<META http-equiv='refresh' content='1;URL=snippet.php?id=". $id ."'>";
break;

//edit snippet
case edit:
$id=$_GET['id'];
  $result = mysqli_query($c,"SELECT * FROM snippet WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);
if ($_SESSION['id'] == $row['user_id']) {

if (isset($_POST['post'])) {
$subject=htmlspecialchars($_POST['subject']);
$type=htmlspecialchars($_POST['lang']);
$code=htmlspecialchars($_POST['snippet']);
if ($code == "") { print "You need to enter a code"; }
elseif ($subject == "") { print "You need to enter a Name"; }
else {
if ($type == "") { $type="mIRC"; }
if ($type != "mIRC" && $type != "PHP" && $type != "ASP" && $type != "Java" && $type !="C" && $type != "C++") {
print "Hacker!";
$type="mIRC";
}
mysqli_query($c,"UPDATE snippet SET snippet_name='$subject', snippet_type='$type', snippet_code='$code' WHERE id='$id' ") or die(mysqli_error($c));
print "Edited Snippet. <META http-equiv='refresh' content='1;URL=snippet.php?id=". $id ."'>";
}
}

//display editing table
else {
print "
<form method='POST' action='snippet.php?action=edit&id=". $id ."'>
<p><h2>Edit Snippet</h2>
Select Language:
<select name='lang'>
<option value='mIRC'>mIRC</option>
<option value='PHP'>PHP</option>
<option value='ASP'>ASP</option>
<option value='Java'>Java</option>
<option value='C'>C</option>
<option value='C++'>C++</option>
<option value='". $row['snippet_type'] ."' selected='selected'>". $row['snippet_type'] ."</option>
</select>
<br>
Snippet Name:<input type='text' name='subject' value='". $row['snippet_name'] ."'><br>
<textarea name='snippet' rows='30' cols='50'>". $row['snippet_code'] ."</textarea><br>
<input type='submit' name='post' value='Edit'>
</p>
</form>
";
}
}

break;

//delete snippet
case delete:
$id=$_GET['id'];
  $result = mysqli_query($c,"SELECT * FROM snippet WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);
if ($_SESSION['id'] == $row['user_id']) {
mysqli_query($c,"DELETE FROM snippet WHERE id='$id' ") or die(mysqli_error($c));
mysqli_query($c,"DELETE FROM comments WHERE snippet_id='$id' ") or die(mysqli_error($c));

print "Snippet Deleted";
}
print "<META http-equiv='refresh' content='1;URL=snippet.php'>";
break;
}

}
//display snippet with comments
if (!isset($_GET['action']) && isset($_GET['id'])) {
$id=$_GET['id'];
  $result = mysqli_query($c,"SELECT * FROM snippet WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);
if (isset($row['id'])) {
  $result = mysqli_query($c,"SELECT * FROM users WHERE id='". $row['user_id'] ."' ") or die(mysqli_error($c));
$row2 = mysqli_fetch_array( $result);

print "<p><h2>". $row['snippet_name'] ."</h2>";
if ($_SESSION['id'] == $row['user_id']) {
print "<a href='snippet.php?action=edit&id=". $id ."'>Edit Snippet</a> | 
<a href='snippet.php?action=delete&id=". $id ."'>Delete Snippet</a><br>";
}
print "
Posted By: ". $row2['user'] ."<br>
Time Posted: ". $row['time'] ."<br>
Language: ". $row['snippet_type'] ."<br>
Snippet Name: ". $row['snippet_name'] ."<br>";
?>
<textarea rows='30' cols='50' name='code' id='txtarea' onClick="SelectAll('txtarea')" readonly='readonly'><?php echo $row['snippet_code']; ?></textarea>
</p>
<p>
<h2>Comments</h2>
<p>
<?php
  $result = mysqli_query($c,"SELECT * FROM comments WHERE snippet_id='$id' ORDER BY time ASC") or die(mysqli_error($c));
while ($row = mysqli_fetch_array( $result)) {
if (isset($row['id'])) {
  $result2 = mysqli_query($c,"SELECT * FROM users WHERE id='". $row['user_id'] ."' ") or die(mysqli_error($c));
$row2 = mysqli_fetch_array( $result2);
print "<p><sup>". $row['time'] ."</sup><br>
Posted by: <a href='profile.php?id=". $row2['id'] ."'>". $row2['user'] ."</a><hr>
". $row['msg'] ."</p>
";
}
}
if (isset($_SESSION['id'])) { 
//make comment
print "<p>
<h3>Post a Comment</p>
<form method='POST' action='snippet.php?action=comment&id=". $id ."'>
<textarea name='comment' cols='50' rows='5'></textarea><br>
<input type='submit' name='post' value='Comment'>
</form>
</p>";
}
}
else {
print "snippet doesn't exist";
}


}
require "footer.php";
?>
