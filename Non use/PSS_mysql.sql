CREATE TABLE users
(
id int NOT NULL AUTO_INCREMENT,
user varchar(16) NOT NULL default ' ',
pass varchar(32) NOT NULL default ' ',
email varchar(50) NOT NULL default ' ',
time datetime  NOT NULL default '0000-00-00 00:00:00',
level tinyint(1) unsigned NOT NULL default '0',
ip varchar(10) NOT NULL default '0',
PRIMARY KEY (id)
);
CREATE TABLE mail
(
id int NOT NULL AUTO_INCREMENT,
user_to int unsigned NOT NULL default '0',
user_from int unsigned NOT NULL default '0',
msg varchar(1000) NOT NULL default ' ',
time datetime  NOT NULL default '0000-00-00 00:00:00',
state tinyint(1) unsigned NOT NULL default '0',
PRIMARY KEY (id)
);
CREATE TABLE ban
(
id int NOT NULL AUTO_INCREMENT,
ip varchar(10) NOT NULL default '0',
reason varchar(250) NOT NULL default ' ',
PRIMARY KEY (id)
);
CREATE TABLE news
(
id int NOT NULL AUTO_INCREMENT,
user_id int unsigned NOT NULL default '0',
msg varchar(1000) NOT NULL default ' ',
time datetime  NOT NULL default '0000-00-00 00:00:00',
PRIMARY KEY (id)
);
CREATE TABLE paste
(
id int NOT NULL AUTO_INCREMENT,
user_id int unsigned NOT NULL default '0',
paste text NOT NULL,
time datetime  NOT NULL default '0000-00-00 00:00:00',
PRIMARY KEY (id)
);
CREATE TABLE snippet
(
id int NOT NULL AUTO_INCREMENT,
user_id int unsigned NOT NULL default '0',
snippet_name varchar(250) NOT NULL default ' ',
snippet_type varchar(50) NOT NULL default ' ',
snippet_code text NOT NULL,
time datetime  NOT NULL default '0000-00-00 00:00:00',
PRIMARY KEY (id)
);
CREATE TABLE comments
(
id int NOT NULL AUTO_INCREMENT,
snippet_id int unsigned NOT NULL default '0',
user_id int unsigned NOT NULL default '0',
msg varchar(500) NOT NULL default ' ',
time datetime  NOT NULL default '0000-00-00 00:00:00',
PRIMARY KEY (id)
);