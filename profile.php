<?php 
/*
*profile.php 
*copyright 2010 Pickle Inc
*Coded By Pickle
*/
require "body.php";
if (!isset($_SESSION['id'])) { print "<META http-equiv='refresh' content='1;URL=login.php'>"; }
else {
//edit profile
if ($_GET['action'] == "edit") {
$id=$_SESSION['id'];
  $result = mysqli_query($c,"SELECT * FROM users WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);
//update mysql
if (isset($_POST['edit'])) {
$oldpass=htmlspecialchars($_POST['oldpass']);
$pass=htmlspecialchars($_POST['newpass']);
$email=htmlspecialchars($_POST['email']);
if (!filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)) { print "E-Mail is not valid"; }
else {

if ($pass != "" && $oldpass != "" && $pass == $oldpass) {
$pass=Encrypt($pass);
mysqli_query($c,"UPDATE users SET pass='$pass' WHERE id='$id' ") or die(mysqli_error($c));
}
if ($email != "") {
mysqli_query($c,"UPDATE users SET email='$email' WHERE id='$id' ") or die(mysqli_error($c));
}

}
print "Edit User";
}
//print table
else {
print "<table align='center' border='1'>
<form method='POST' action='profile.php?action=edit'>
<tr><th colspan='2'>Edit User</th></tr>
<tr><td>Email:</td><td><input type='text' name='email' value='". $row['email'] ."' /></td></tr>
<tr><td class='alt'>Old Password</td><td><input type='password' name='oldpass'/></td></tr>
<tr><td>New Password</td><td><input type='password' name='newpass'/></td></tr>
<tr><th colspan='2'><input type='submit' name='edit' value='edit' /></th></tr>
</form>
</table>
";
}
}
//display profile
else {
$id=$_GET['id'];
if (!isset($id)) {
$id=$_SESSION['id'];
}
  $result = mysqli_query($c,"SELECT * FROM users WHERE id='$id' ") or die(mysqli_error($c));
$row = mysqli_fetch_array( $result);
  $result = mysqli_query($c,"SELECT * FROM snippet WHERE user_id='$id' ") or die(mysqli_error($c));
while ($row2 = mysqli_fetch_array( $result)) { $s++; }
  $result = mysqli_query($c,"SELECT * FROM comments WHERE user_id='$id' ") or die(mysqli_error($c));
while ($row2 = mysqli_fetch_array( $result)) { $co++; }
  $result = mysqli_query($c,"SELECT * FROM paste WHERE user_id='$id' ") or die(mysqli_error($c));
while ($row2 = mysqli_fetch_array( $result)) { $p++; }
if ($co <= 0) { $co=0; } 
if ($s <= 0) { $s=0; } 
if ($p <= 0) { $p=0; } 

if ($_SESSION['id'] == $id) { print "<a href='profile.php?action=edit'>Edit Profile</a>"; }
else { print "<a href='inbox.php?action=send'>Message</a>"; }

print "<table align='center' border='1'>
<tr><th colspan='2'>". $row['user'] ."</th></tr>
<tr><td>User:</td><td>". $row['user'] ."</td></tr>
<tr><td class='alt'>Time Registered:</td><td class='alt'>". $row['time'] ."</td></tr>
<tr><td>Email:</td><td>". $row['email'] ."</td></tr>";
if ($row['level'] == 2) { $level="Admin"; }
else { $level="Member"; }
print "<tr><td class='alt'>Access:</td><td class='alt'>". $level ."</td></tr>
<tr><th colspan='2'>Stats:</th></tr>
<tr><td>Total Comments:</td><td>". $co ."</td></tr>
<tr><td class='alt'>Total Snippets:</td><td class='alt'>". $s ."</td></tr>
<tr><td>Total Pastes:</td><td>". $p ."</td></tr>
</table>";

}
}
require "footer.php";
?>
